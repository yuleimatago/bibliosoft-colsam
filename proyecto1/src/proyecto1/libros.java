/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto1;

import clases.conectar;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author colsam-15
 */
public class libros extends javax.swing.JFrame {
     DefaultTableModel modelo;
    /**
     * Creates new form libros
     */
    public libros() {
        initComponents();
        setResizable(false);
        setLocationRelativeTo(null);
        setResizable(false);//para que la pantalla no se puede ampliar 
        setTitle("Ventana de seguridad - BibliotecaSoft COLSAM.");
        this.setLocationRelativeTo(libros.this);
        //this.setResizable(false);
        Consultar();
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtcod = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtname = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        txtauthor = new javax.swing.JTextField();
        txtshelf = new javax.swing.JTextField();
        txtquantity = new javax.swing.JTextField();
        txtgender = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtsubshelf = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(990, 580));
        getContentPane().setLayout(null);

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("NOMBRE  :");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(240, 80, 130, 30);

        jLabel8.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("AUTOR   :");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(260, 130, 120, 28);

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("CÓDIGO:");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(250, 30, 130, 30);

        txtcod.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtcodCaretUpdate(evt);
            }
        });
        txtcod.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcodActionPerformed(evt);
            }
        });
        txtcod.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtcodKeyTyped(evt);
            }
        });
        getContentPane().add(txtcod);
        txtcod.setBounds(390, 30, 120, 22);

        jLabel9.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("ESTANTE  :");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(240, 180, 140, 30);

        jLabel10.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(240, 240, 240));
        jLabel10.setText("GENERO    :");
        getContentPane().add(jLabel10);
        jLabel10.setBounds(540, 150, 160, 28);

        txtname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnameActionPerformed(evt);
            }
        });
        getContentPane().add(txtname);
        txtname.setBounds(390, 80, 120, 22);

        jLabel11.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(240, 240, 240));
        jLabel11.setText("CANTIDAD  :");
        getContentPane().add(jLabel11);
        jLabel11.setBounds(540, 80, 160, 30);

        jButton1.setBackground(new java.awt.Color(0, 0, 0));
        jButton1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("VOLVER");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1);
        jButton1.setBounds(870, 10, 110, 20);

        jButton2.setBackground(new java.awt.Color(0, 0, 0));
        jButton2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jButton2.setForeground(new java.awt.Color(255, 255, 255));
        jButton2.setText("GUARDAR");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2);
        jButton2.setBounds(450, 280, 130, 40);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTable1.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                jTable1AncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        jScrollPane1.setViewportView(jTable1);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(0, 470, 1010, 110);
        getContentPane().add(txtauthor);
        txtauthor.setBounds(390, 140, 120, 22);
        getContentPane().add(txtshelf);
        txtshelf.setBounds(390, 190, 120, 22);

        txtquantity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtquantityActionPerformed(evt);
            }
        });
        txtquantity.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtquantityKeyTyped(evt);
            }
        });
        getContentPane().add(txtquantity);
        txtquantity.setBounds(720, 80, 110, 22);

        txtgender.setBackground(new java.awt.Color(0, 0, 0));
        txtgender.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        txtgender.setForeground(new java.awt.Color(255, 255, 255));
        txtgender.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Literatura", "Economia ", "C.Sociales", "Religión", "Inglés", "Matemáticas", "Lengua Castellana", "C.Naturales", "Psicologia", "Pedagogia", "P. de grado", "Enciclopedia" }));
        txtgender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtgenderActionPerformed(evt);
            }
        });
        getContentPane().add(txtgender);
        txtgender.setBounds(730, 150, 180, 20);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/logo.png"))); // NOI18N
        jLabel1.setText("jLabel1");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(-10, 0, 200, 210);

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("SUB-ESTANTE  :");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(530, 210, 200, 40);

        txtsubshelf.setBackground(new java.awt.Color(0, 0, 0));
        txtsubshelf.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        txtsubshelf.setForeground(new java.awt.Color(255, 255, 255));
        txtsubshelf.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "A", "B", "C", "D", "E" }));
        txtsubshelf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtsubshelfActionPerformed(evt);
            }
        });
        getContentPane().add(txtsubshelf);
        txtsubshelf.setBounds(740, 220, 50, 30);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/BIBLIOTECA.jpg"))); // NOI18N
        jLabel4.addContainerListener(new java.awt.event.ContainerAdapter() {
            public void componentRemoved(java.awt.event.ContainerEvent evt) {
                jLabel4ComponentRemoved(evt);
            }
        });
        getContentPane().add(jLabel4);
        jLabel4.setBounds(0, 0, 990, 580);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       VistaPrincipal VistaPrincipal= new VistaPrincipal();
     VistaPrincipal.setVisible(true);
     this.dispose();
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtgenderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtgenderActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtgenderActionPerformed

    private void jTable1AncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_jTable1AncestorAdded

        // TODO add your handling code here:
    }//GEN-LAST:event_jTable1AncestorAdded

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        
        Insertar();
        Consultar();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void txtquantityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtquantityActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtquantityActionPerformed

    private void txtnameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnameActionPerformed

    private void txtcodActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcodActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcodActionPerformed

    private void jLabel4ComponentRemoved(java.awt.event.ContainerEvent evt) {//GEN-FIRST:event_jLabel4ComponentRemoved
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel4ComponentRemoved

    private void txtsubshelfActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtsubshelfActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtsubshelfActionPerformed

    private void txtcodCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtcodCaretUpdate
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcodCaretUpdate

    private void txtcodKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcodKeyTyped
        char c = evt.getKeyChar();
        
        if(c<'0' || c>'9') evt.consume();
    }//GEN-LAST:event_txtcodKeyTyped

    private void txtquantityKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtquantityKeyTyped
         char c = evt.getKeyChar();
        
        if(c<'0' || c>'9') evt.consume();//para colocar solo numeros 
    }//GEN-LAST:event_txtquantityKeyTyped

    public void Consultar() {
        modelo=new DefaultTableModel();
        modelo.addColumn("Codigo");
        modelo.addColumn("Nombre");
        modelo.addColumn("Autor");
        modelo.addColumn("Estante");
        modelo.addColumn("Cantidad");
        modelo.addColumn("Genero");
        modelo.addColumn("Sub-Estante");
        this.jTable1.setModel(modelo);
    conectar cc= new conectar();
        Connection cn = cc.conexion();
        Statement st;
         try {
             st = cn.createStatement();
             String sql = "SELECT cod, title, author, gender, quantity, editorial, shelf FROM books;";
        ResultSet rs = st.executeQuery(sql);
        while(rs.next()){
            String filas[] = new String[7];
            for(int i=0; i<7;i++){
                filas[i]=rs.getString(i+1);
            }
            modelo.addRow(filas);
            jTable1.setModel(modelo);
        }
        cn.close();
         } catch (SQLException ex) {
             Logger.getLogger(libros.class.getName()).log(Level.SEVERE, null, ex);
         }
    }

    
    public void Insertar() {
    conectar cc= new conectar();
        Connection cn = cc.conexion();
        PreparedStatement pst;
         try {
             String sql = "INSERT INTO books (cod, title, author, gender, quantity, editorial, shelf) VALUES (?, ?, ?, ?, ?, ?, ?);";
              pst = cn.prepareStatement(sql);
                pst.setInt    (1, Integer.parseInt(txtcod.getText()));
                pst.setString (2, txtname.getText());
                pst.setString (3, txtauthor.getText());
                pst.setString (4, txtshelf.getText());
                pst.setInt    (5, Integer.parseInt(txtquantity.getText()));
                pst.setString (6, (String)txtgender.getSelectedItem());
                pst.setString (7,(String)txtsubshelf.getSelectedItem());
                
                pst.executeUpdate();

        cn.close();
         } catch (SQLException ex) {
             Logger.getLogger(libros.class.getName()).log(Level.SEVERE, null, ex);
         }
         
        
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(libros.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(libros.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(libros.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(libros.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new libros().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField txtauthor;
    private javax.swing.JTextField txtcod;
    private javax.swing.JComboBox<String> txtgender;
    private javax.swing.JTextField txtname;
    private javax.swing.JTextField txtquantity;
    private javax.swing.JTextField txtshelf;
    private javax.swing.JComboBox<String> txtsubshelf;
    // End of variables declaration//GEN-END:variables
}
